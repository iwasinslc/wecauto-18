@extends('layouts.profile')
@section('title', 'Проверить заявку')

@section('content')
    <div class="cashback">
        <section>
            <div class="container">
                <h2 class="lk-title">
                    @yield('title')
                </h2>
                <div class="claim-details content-block">
                    <div class="claim">
                        {{--<p class="claim__id">0001
                        </p>--}}
                        <p class="claim__date">{{$cashbackRequest->created_at}}
                        </p>
                        @if($cashbackRequest->approved === null)
                            <p class="status">
                                <svg class="svg-icon">
                                    <use href="assets/icons/sprite.svg#icon-hourglass"></use>
                                </svg><span>На рассмотрении</span>
                            </p>
                        @elseif($cashbackRequest->approved)
                            <p class="status status--gray">
                                <svg class="svg-icon">
                                    <use href="assets/icons/sprite.svg#icon-check"></use>
                                </svg><span>Одобрена</span>
                            </p>
                        @else
                            <p class="status status--warning">
                                <svg class="svg-icon">
                                    <use href="assets/icons/sprite.svg#icon-uncheck"></use>
                                </svg><span>Отклонена</span>
                            </p>
                        @endif
                    </div>
                    <div class="cashback-form">
                        <div class="cashback-form__top">
                            <div style="width: 100%">
                                <div class="row">
                                    <div class="col">
                                        <div class="field field--gray">
                                            <label>Логин:</label>
                                            <input type="text" value="{{$user->login}}" name="login" placeholder="Логин" disabled>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="field field--gray">
                                            <label>Емейл:</label>
                                            <input type="text" value="{{$user->email}}" name="email" placeholder="mail@gmail.com" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="field field--gray">
                                            <label>Telegram:</label>
                                            <input type="text" name="telegram" value="{{ $cashbackRequest->telegram }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="claim-details__row">
                        <div class="claim-details__col">
                            <h6 class="claim-details__subtitle">Документы (свидетельство о регистрации ТС, договор купли-продажи ТС):
                            </h6>
                            <ul class="file-list">
                                @foreach ($documents as $document)
                                    <li>
                                        <a class="file-item" href="{{ $document['url'] }}" download="download">{{ $document['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            @if(!$cashbackRequest->documents_checked)
                            <div>
                                <form method="POST" action="{{ route('profile.cashback-result.update', ['id' => $cashbackRequest->id]) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="cashback-form">
                                        <input type="text" hidden name="documents_checked" value="1">
                                        <button class="btn btn--warning">{{__('Подтвердить документы')}}</button>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>
                        <div class="claim-details__col">
                            <h6 class="claim-details__subtitle">Видео:
                            </h6>
                            <div style="margin-bottom: 25px;">
                                <a href="{{ $cashbackRequest->video_link }}" target="_blank">{{ $cashbackRequest->video_link }}</a>
                            </div>
                            @if(!$cashbackRequest->video_checked)
                                <div>
                                    <form method="POST" action="{{ route('profile.cashback-result.update', ['id' => $cashbackRequest->id]) }}">
                                        @csrf
                                        @method('PUT')
                                        <div class="cashback-form">
                                            <input type="text" hidden name="video_checked" value="1">
                                            <button class="btn btn--warning">{{__('Подтвердить видео')}}</button>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <h2 class="lk-title">Результат рассмотрения
                </h2>
                <div class="content-block">
                    @if($cashbackRequest->approved !== null)
                        <div class="typography">
                            Ответ: {{ $cashbackRequest->result }}
                        </div>
                    @else
                        <form method="POST" action="{{ route('profile.cashback-result.update', ['id' => $cashbackRequest->id]) }}">
                            @csrf
                            @method('PUT')
                            <div class="cashback-form">
                                <div class="field field--gray">
                                    <label>{{ __('Результат') }}</label>
                                    <textarea name="result" required>{{ old('result', $cashbackRequest->result) }}</textarea>
                                </div>
                                <div class="field field--gray">
                                    <label for="approved">Статус</label>
                                    <label class="checkbox">
                                        <input type="radio" name="approved" value="1" {{$cashbackRequest->result === true ? 'checked' : ''}}>
                                        <span>Подтвердить</span>
                                    </label>
                                    <label class="checkbox">
                                        <input type="radio" name="approved" value="0" {{$cashbackRequest->result === false ? 'checked' : ''}}>
                                        <span>Отменить</span>
                                    </label>
                                </div>
                                <button class="btn btn--warning">
                                    {{ __('Сохранить результат') }}
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection