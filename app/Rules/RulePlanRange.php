<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Rate;

/**
 * Class RulePlanRange
 * @package App\Rules
 */
class RulePlanRange implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  float  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rate = Rate::find(request()->get('rate_id'));

        if (null === $rate) {
            return false;
        }

        return $rate->min <= $value && $value <= $rate->max;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The amount is not included in the license limitation');
    }
}
