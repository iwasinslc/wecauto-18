<?php

namespace App\Http\Requests\Api;

use App\Rules\RuleUUIDEqual;
use Illuminate\Foundation\Http\FormRequest;

/**
 *  @OA\Parameter(
 *     parameter="user_id",
 *     name="user_id",
 *     description="Filter by user id",
 *     required=true,
 *     in="query",
 *     @OA\Schema(
 *         type="string"
 *     )
 *  ),
 */
class RequestDataTable extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => [
                'required',
                'string',
                new RuleUUIDEqual(),
                'exists:App\Models\User,id'
            ]
        ];
    }
}
