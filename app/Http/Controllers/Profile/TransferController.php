<?php
namespace App\Http\Controllers\Profile;

use App\Events\NotificationEvent;
use App\Facades\TransferF;
use App\Facades\WithdrawF;
use App\Http\Controllers\Controller;

use App\Http\Requests\RequestTransfer;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Withdraw;
use App\Services\TransactionService;

/**
 * Class TransferController
 * @package App\Http\Controllers\Profile
 */
class TransferController extends Controller
{

    public function index()
    {
        return view('profile.transfer');
    }


    public function transfer(RequestTransfer $request)
    {
//        if (!user()->hasRole(['root']))
//        {
//            return back()->with('error', __('Function dont working'));
//        }

        $user = user();
        /**
         * @var Wallet $sender_wallet
         */
        $sender_wallet = user()->wallets()->find($request->wallet_id);

        $currency = $sender_wallet->currency;

        if ($currency->code=='FST')
        {
            $limit = $user->sellLimit()*rate('USD', 'FST');

            if ($limit<$request->amount)
            {
                return redirect()->route('profile.transfer')->with('error',__('Limits is done'));
            }
        }


//        if ($currency->code=='WEC')
//        {
//            $limit = $user->deposit_sell_limit*rate('USD', 'WEC');
//
//            if ($limit<$request->amount)
//            {
//                return redirect()->route('profile.transfer')->with('error',__('Limits is done'));
//            }
//        }

        //$user->removeDepositSellLimit($amount*$rate);

        /**
         * @var User $receiver
         */
        $receiver = User::where('email', $request->email)->where('login', $request->login)->first();
        if ($receiver===null||$receiver->id==user()->id)
        {
            return redirect()->route('profile.transfer')->with('error', __('User not found'));
        }


        if ($currency->code=='FST')
        {
            $limit = $receiver->buyLimit()*rate('USD', 'FST');

            if ($limit<$request->amount||!$receiver->activeLicence())
            {
                return redirect()->route('profile.transfer')->with('error',__('Receiver Limits is done'));
            }
        }

//        if ($receiver->is_new==0)
//        {
//            return back()->with('error', __('This user cannot receive transfers'));
//        }

        $receiver_wallet = $receiver->wallets()
            ->where('payment_system_id', $sender_wallet->payment_system_id)
            ->where('currency_id', $sender_wallet->currency_id)
            ->first();

        if (empty($receiver_wallet)) {
            $receiver_wallet = Wallet::newWallet($receiver, $sender_wallet->currency, $sender_wallet->paymentSystem);
        }
        $with_confirmation = false;
        try {
            TransferF::create($sender_wallet, $receiver_wallet, $request->amount, $with_confirmation);

            if ($currency->code=='FST')
            {
                $user->removeLimitsSell($request->amount, $currency->code);
                $receiver->removeLimitsBuy($request->amount, $currency->code);
            }

//            if ($currency->code=='WEC')
//            {
//                $user->removeDepositSellLimit($request->amount*rate('WEC', 'USD'));
//            }

            if($with_confirmation) {
                return back()->with('success', __('Confirm transfer by link in the email.'));
            } else {
                return back()->with('success', __('Send success'));
            }
        } catch(\Exception $e) {
            return redirect()->route('profile.transfer')->with('error', $e->getMessage());
        }

        /** @var User $userSender */
        //$userSender = $sender_wallet->user;
//
//        /** @var User $userReceiver */
//        $userReceiver = $receiver_wallet->user;
//
//        try {
//            $userSender->sendTelegramNotification('transfer_sent', [
//                'senderWallet' => $sender_wallet,
//                'receiverWallet' => $receiver_wallet,
//                'amount' => $request->amount,
//            ]);
//        } catch (\Exception $e) {
//            // ...
//        }
//
//        try {
//            $userReceiver->sendTelegramNotification('transfer_receiver', [
//                'senderWallet' => $sender_wallet,
//                'receiverWallet' => $receiver_wallet,
//                'amount' => $request->amount,
//            ]);
//        } catch (\Exception $e) {
//            // ...
//        }

    }

    public function confirmTransaction($type, $confirmation_code, TransactionService $transactionService) {
        try {
            switch ($type) {
                case $transactionService::TYPE_WITHDRAW:
                    WithdrawF::confirmByCode($confirmation_code);
                    $status = __('Withdraw confirmed successfully');
                    break;
                case $transactionService::TYPE_TRANSFER:
                    TransferF::confirmByCode($confirmation_code);
                    $status = __('Transfer confirmed successfully');
                    break;
                default:
                    throw new \Exception('Unknown transaction type');
            }
        } catch (\Throwable $e) {
            $status = $e->getMessage();
        }
        return view('customer.message_block', [
            'message' => $status
        ]);
    }
}
