<?php
namespace App\Http\Controllers\Payment;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\CoinpaymentsModule;
use App\Modules\PaymentSystems\EtherApiModule;
use App\Modules\PaymentSystems\PrizmApiModule;
use App\Modules\PaymentSystems\WebCoinApiModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class EtherApiController
 * @package App\Http\Controllers\Payment
 */
class PrizmApiController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function topUp()
    {
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = session('topup.payment_system');

        /** @var Currency $currency */
        $currency = session('topup.currency');

        if (empty($paymentSystem) || empty($currency)) {
            return redirect()->route('profile.topup')->with('error', __('Can not process your request, try again.'));
        }

        $amount = abs(session('topup.amount'));
        $user          = Auth::user();

        $wallet = getUserWallet($currency->code, $paymentSystem->code);

        if (empty($wallet)) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        if (null == $wallet->address) {
            $address = PrizmApiModule::getAddress();

            $wallet->address = $address;
            $wallet->save();
            $wallet->fresh();
        }




        return view('ps.prizmapi', [
            'currency' => $currency->code,
            'amount' => $amount,
            'statusUrl' => route('perfectmoney.status'),
            'user' => $user,
            'wallet' => $wallet,
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function status(Request $request)
    {



        \Log::alert('status--'.print_r($request->all(),true));

        $rawRequest = json_decode(file_get_contents('php://input'), true);

        if ($rawRequest === FALSE || empty($rawRequest)|| !isset($rawRequest['prizmapi.net'])) {
            \Log::critical('Prizm. Strange request from: '.$request->ip().', Error reading POST data. '.print_r($request->all(),true));
            return response('ok');
        }



        $sign = sha1(implode(':', array(
            $rawRequest['type'],
            $rawRequest['date'],
            $rawRequest['from'],
            $rawRequest['to'],
            $rawRequest['tag'],
            $rawRequest['amount'],
            $rawRequest['fee'],
            $rawRequest['txid'],
            $rawRequest['label'],
            env('PRIZM_API_KEY') // токен доступа к API
        )));

        if ($sign !== $rawRequest['sign']) {
            \Log::critical('WEC API. Strange request from: '.$request->ip().', HMAC signature does not match. '.print_r($request->all(),true));
            return response('ok');
        }

        if (!$request->has('amount') ||

            !$request->has('txid') ||
            !$request->has('type') ||
            $request->type!='in' ||
            !$request->has('tag')) {
            \Log::info('WEC API. Strange request from: '.$request->ip().'. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = PaymentSystem::where('code', 'prizm')->first();
        /** @var Currency $currency */
        $currency      = Currency::where('code', strtoupper("PZM"))->first();

        if (null == $currency) {
            \Log::critical('Strange request from: '.$request->ip().'. Currency not found. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }


//        /** @var User $user */
//        $user = User::where('wtp_deposit_address', $request->to)->first();
//
//        if (null == $user) {
//            \Log::critical('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));
//            return response('ok');
//        }
        /** @var Wallet $wallet */
        $wallet = Wallet::where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->where('address', $request->tag)
            ->first();

        if ($wallet===null)
        {
            \Log::critical('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));
        }


                /** @var User $user */
        $user = $wallet->user;



        /** @var Transaction $transaction */

        $transaction = Transaction::where('batch_id', $request->txid)->where('currency_id', $currency->id)->where('payment_system_id', $paymentSystem->id)->first();
        if ($transaction===null)
        {
            $transaction = Transaction::enter($wallet, $request->amount);

            if (null!==$transaction)
            {

                $transaction->batch_id = $request->txid;
                $transaction->result = 'complete';
                $transaction->source = $rawRequest['to'].':'.$rawRequest['tag'];
                $transaction->save();
                $commission = $transaction->amount * 0.01 * $transaction->commission;

                $wallet->refill(($transaction->amount-$commission), $transaction->source);
                $transaction->update(['approved' => true]);

                NotificationEvent::dispatch($user, 'notifications.wallet_refiled', [
                    //'id'=>$user->id,
                    'user_id'=>$user->id,
                    'amount'=>$transaction->amount-$commission,
                    'currency'=>$transaction->currency->code
                ]);




                PrizmApiModule::getBalances(); // обновляем баланс нашего внешнего кошелька в БД
                return response('OK');

            }
        }






        \Log::critical('Prizm transaction is not passed. IP: '.$request->ip().'. '.print_r($request->all(), true));
        return response('OK');
    }
}
