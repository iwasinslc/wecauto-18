<?php

use App\Models\Licences;

/**
 * @param $userId |null
 * @return string|null
 * @throws Exception
 */
function getAdminD3V3ReferralsTree($userId = null)
{
    if (null === $userId) {
        return null;
    }

    return cache()->remember('a.' . $userId . '.d3v3ReferralsTree', getCacheALifetime('d3v3ReferralsTree'), function () use ($userId) {
        return json_encode(\App\Models\User::getD3V3ReferralsTree(\App\Models\User::find($userId)));
    });
}

/**
 * @return int
 * @throws Exception
 */
function getAdminWithdrawRequestsCount(): int
{
    return cache()->remember('a.withdrawRequestsCount', getCacheALifetime('withdrawRequestsCount'), function () {
        /** @var \App\Models\TransactionType $transactionWithdrawalType */
        return \App\Models\Withdraw::where('status_id', \App\Models\TransactionStatus::STATUS_CONFIRMED_BY_EMAIL)
            ->count();
    });
}

/**
 * @return int
 * @throws Exception
 */
function getAdminCashbackRequestsCount(): int
{
    return cache()->rememberForever('a.cashbackRequestsCount', function() {
        return \App\Models\CashbackRequest::query()->whereNull('approved')->count();
    });
}

/**
 * @param string|null $typeId
 * @return int
 * @throws Exception
 */
function getAdminTransactionsCount($typeId = null): int
{
    return cache()->remember('a.transactionsCount', getCacheALifetime('transactionsCount'), function () use ($typeId) {
        if (null !== $typeId) {
            return \App\Models\Transaction::where('type_id', $typeId)->count();
        }
        return \App\Models\Transaction::count();
    });
}

/**
 * @return array
 *
 * :)
 */
function getAdminMergeDepositedAndWithdrew(): array
{
    return [
        'deposited' => getTotalDeposited(),
        'withdrew' => getTotalWithdrew(),
    ];
}

/**
 * @param \Carbon\Carbon $date
 * @param int $limit
 * @return array
 * @throws Exception
 */
function getAdminDepositsSumClosingAtDate($date = null, $limit = 100): array
{
    if (null == $date || (false == $date instanceof \Carbon\Carbon)) {
        return [];
    }

    return cache()->tags('depositsSumClosingAtDate')->remember('a.depositsSumClosingAtDate.' . $date . '.limit-' . $limit, getCacheALifetime('depositsSumClosingAtDate'), function () use ($date, $limit) {
        $depositsAtDate         = \App\Models\Deposit::where('datetime_closing', 'like', \Carbon\Carbon::parse($date)->toDateString().'%');
        $closingDeposits        = [];
        $closingDepositsSum     = [];

        foreach ($depositsAtDate as $deposit) {
            if (!isset($closingDepositsSum[$deposit->currency->code])) {
                $closingDepositsSum[$deposit->currency->code] = 0;
            }

            $closingDeposits[] = $deposit;
            $closingDepositsSum[$deposit->currency->code] += $deposit->invested;
        }
        return [
            'deposits' => $closingDeposits,
            'total' => $closingDepositsSum,
        ];
    });
}

/**
 * @return array
 * @throws Exception
 */
function getAdminPlanPopularity(): array
{
    return cache()->remember('a.plansPopularity', getCacheALifetime('plansPopularity'), function () {
        $popularity = [];
        $plans = getTariffPlans();

        foreach ($plans as $plan) {
            $depositsSum = \App\Models\Deposit::where('rate_id', $plan['id'])->count();
            $popularity[$plan['id']] = $plan;
            $popularity[$plan['id']]['depositsSum'] = $depositsSum;
        }

        return $popularity;
    });
}



/**
 * @param int $days
 * @param string $currency
 * @return array
 * @throws Exception
 */
function getAdminLicensesTrafficStatistic($days = null)
{
    if (null === $days) {
        return [];
    }

    return cache()->tags('licencesTrafficStatistic')->remember('a.licencesTrafficStatistic1.days-' . $days , now()->addHours(3), function () use ($days) {

        $licences = Licences::orderBy('id')->get();

        $daysArray = [];

        $type = \App\Models\TransactionType::getByName('buy_license');


        for ($i = $days; $i > 0; $i--) {
            $day = \Carbon\Carbon::now()->subDays($i);
            $daysArray[$day->format('Y-m-d')] = cache()->tags('licencesTrafficStatistic.specificDay')->rememberForever('a.licencesTrafficStatistic1.days-' . $days  . '.date-' . $day->toFormattedDateString(), function () use ( $type, $day, $licences) {
                $stat = \App\Models\Transaction::select('source', DB::raw('count(*) as total'))
                    ->where('type_id', $type->id)
                    ->where('created_at', '>=', $day->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', $day->endOfDay()->toDateTimeString())
                    ->where('approved', 1)
                    ->groupBy('source')
                    ->orderBy('source')
                    ->get()->groupBy('source')->toArray();


                $data = [];


                foreach ($licences as $license) {
                    $data[$license->id] = isset($stat[$license->id]) ? $stat[$license->id][0]['total'] : 0;
                }

                return $data;
            });
        }
        return $daysArray;
    });
}

/**
 * @param int $days
 * @param string $currency
 * @return array
 * @throws Exception
 */
function getAdminMoneyTrafficStatistic($days = null, $currency = null)
{
    if (null === $days || null === $currency) {
        return [];
    }

    return cache()->tags('moneyTrafficStatistic')->remember('a.moneyTrafficStatistic.days-' . $days . '.currency-' . $currency, getCacheALifetime('moneyTrafficStatistic'), function () use ($days, $currency) {
        $daysArray = [];
        $currency = \App\Models\Currency::where('code', $currency)->first();
        $typeEnter = \App\Models\TransactionType::getByName('enter');

        $typeBuy = \App\Models\TransactionType::getByName('buy_wec');

        if (!isset($currency, $typeEnter)) {
            return null;
        }

        for ($i = $days; $i > 0; $i--) {
            $day = \Carbon\Carbon::now()->subDays($i);
            $daysArray[$day->format('Y-m-d')] = cache()->tags('moneyTrafficStatistic.specificDay')->rememberForever('a.moneyTrafficStatistic.days-' . $days . '.currency-' . $currency . '.date-' . $day->toFormattedDateString(), function () use ($days, $currency, $typeEnter, $typeBuy ,$day) {
                $enter = \App\Models\Transaction::where('currency_id', $currency->id)
                    ->where('type_id', $typeEnter->id)
                    ->where('created_at', '>=', $day->format('Y-m-d') . ' 00:00:01')
                    ->where('created_at', '<=', $day->format('Y-m-d') . ' 23:59:59')
                    ->where('approved', 1)
                    ->sum('amount');

                $withdrew = \App\Models\Withdraw::where('currency_id', $currency->id)
                    ->where('created_at', '>=', $day->format('Y-m-d') . ' 00:00:01')
                    ->where('status_id', \App\Models\TransactionStatus::STATUS_APPROVED)
                    ->where('created_at', '<=', $day->format('Y-m-d') . ' 23:59:59')
                    ->sum('amount');


                $buy = \App\Models\OrderPiece::query()
                    ->where('main_currency_id', \App\Models\Currency::getByCode('WEC')->id)
                    ->where('currency_id', \App\Models\Currency::getByCode('USD')->id)
                    ->where('type', \App\Models\ExchangeOrder::TYPE_SELL)
                    ->where('created_at', '>=', $day->format('Y-m-d') . ' 00:00:01')
                    ->where('created_at', '<=', $day->format('Y-m-d') . ' 23:59:59')
                    ->sum('rate_amount');
//                $buy = \App\Models\Transaction::where('currency_id', $currency->id)
//                    ->where('type_id', $typeBuy->id)
//                    ->where('approved', 1)
//                    ->where('created_at', '>=', $day->format('Y-m-d') . ' 00:00:01')
//                    ->where('created_at', '<=', $day->format('Y-m-d') . ' 23:59:59')
//                    ->sum('amount');
                return [
                    'enter' => round($enter, $currency->precision),
                    'withdrew' => round($withdrew, $currency->precision),
//
                   'buy' => round($buy, $currency->precision),
                ];
            });
        }
        return $daysArray;
    });
}

/**
 * @param null $days
 * @return array
 * @throws Exception
 */
function getAdminUsersActivityStatistic($days = null)
{
    if (null === $days) {
        return [];
    }

    return cache()->tags('usersActivityStatistic')->remember('a.usersActivityStatistic.days-' . $days, getCacheALifetime('usersActivityStatistic'), function () use ($days) {
        $daysArray = [];

        for ($i = $days; $i > 0; $i--) {
            $day = \Carbon\Carbon::now()->subDays($i);
            $daysArray[$day->format('Y-m-d')] = cache()->tags('usersActivityStatistic.specificDay')->remember('a.usersActivityStatistic.days-' . $days . '.date-' . $day->toDateString(), getCacheALifetime('usersActivityStatistic'), function () use ($days, $day) {
                return [
                    'visitors' => \App\Models\PageViews::select('user_ip')
                        ->distinct()
                        ->where('created_at', '>=', $day->format('Y-m-d') . ' 00:00:01')
                        ->where('created_at', '<=', $day->format('Y-m-d') . ' 23:59:59')
                        ->count(['user_ip']),
                    'pageViews' => \App\Models\PageViews::where('created_at', '>=', $day->format('Y-m-d') . ' 00:00:01')
                        ->where('created_at', '<=', $day->format('Y-m-d') . ' 23:59:59')
                        ->count(),
                ];
            });
        }
        return $daysArray;
    });
}

/**
 * @param null $currency
 * @param null $type_name
 * @return int
 * @throws Exception
 */
function getTransactionStatistic($currency = null, $type_name= null)
{
    if ( null === $currency|| null===$type_name) {
        return 0;
    }


        $currency = \App\Models\Currency::getByCode($currency);
        $type = \App\Models\TransactionType::getByName($type_name);


        if (null === $currency || null === $type) {
            return null;
        }

    $sum = cache()->tags('getTransactionStatistic')->remember('getTransactionStatistic'.$currency->id.$type->id,  now()->addMinutes(30), function () use ($currency, $type) {

        return \App\Models\Transaction::where('currency_id', $currency->id)
            ->where('type_id', $type->id)
            ->where('created_at', '>=', now()->startOfDay()->toDateTimeString())
            ->where('created_at', '<=', now()->toDateTimeString())
            ->sum('amount');

    });



        return $sum;

}